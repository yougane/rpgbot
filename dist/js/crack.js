var http = require("http"),
    EventEmitter = require("events").EventEmitter,
    fs = require('fs');

function crack(src) {
    var captcha, image, sum=Number.MAX_VALUE, solution;
    var evt = new EventEmitter();

    captcha = new Image();
    captcha.onload = function() {
        var max = captcha.height / 250;
        var colors = new Array(max);
        var type;
        var freqs = new Array(max);
        var sums = new Array(max);
        var accs = [];
        var ans = new Array(max);
        for (var i = 0; i < max; i++) {
            image = takeImage(captcha, max,  i);
            res = sobelSum(image);
            s = res.sum;
            accs.push(s);
            ans[i] = s;
            //colors[i] = new Int32Array(6);
            //colors[i].set(res.colors);
            if (s < sum) {
                sum = s;
                solution = i;
            }
            freqs[i] = colorsFreqs(image);
            sums[i] = colorsSums(image);
        }

        if (isColoredCaptcha(freqs)) {
            type = 'colors';
        } else {
            type = 'spiral';
        }
        console.log('Type: ' + type);
        if (type === 'colors') {
            evt.emit('failure');
        } else {
            //console.log(accs);
            var diffs = realsol(accs);
            diffs[0] = diffs[max-1] = false;
            //console.log(diffs);
            var c1, c2;
            for (var i = 0;; i++) {
                if (diffs[(solution + i) % max] === true) {
                    c1 = (solution + i) % max;
                    break;
                }
            }
            for (var i = 0;; i++) {
                if (diffs[(solution - i + max) % max] === true) {
                    c2 = (solution - i + max) % max;
                    break;
                }
            }
            if (c1 == c2) {
                solution = c1;
            } else {
                //var n1 = (accs[c1+2] - accs[c1+2]) - (accs[c1-1] - accs[c1-2]),
                //n2 = (accs[c2+2] - accs[c2+2]) - (accs[c2-1] - accs[c2-2]),
                //solution = n1 > n2 ? c1 : c2;
                //var ang1 = angle(c1+1, accs[c1+1], c1, accs[c1]),
                //ang2 = angle(c1+1, accs[c1+1], c1+2, accs[c1+2]),
                //ang3 = angle(c1-1, accs[c1-1], c1-2, accs[c1-2]),
                //ang4 = angle(c1-1, accs[c1], c1, accs[c1]),
                //ang5 = angle(c2+1, accs[c2+1], c2, accs[c2]),
                //ang6 = angle(c2+1, accs[c2+1], c2+2, accs[c2+2]),
                //ang7 = angle(c2-1, accs[c2-1], c2-2, accs[c2-2]),
                //ang8 = angle(c2-1, accs[c2], c2, accs[c2]);
                //var an1 = ang1 - ang2,
                //an2 = ang3 - ang4,
                //an3 = ang5 - ang6,
                //an4 = ang7 - ang8;
                var ang1 = Math.atan((accs[c1] - accs[c1+1]) / -1),
                ang2 = Math.atan(accs[c1+2] - accs[c1+1]),
                ang3 = Math.atan((accs[c1-2] - accs[c1-1]) / -1),
                ang4 = Math.atan(accs[c1] - accs[c1-1]),
                ang5 = Math.atan((accs[c2] - accs[c2+1]) / -1),
                ang6 = Math.atan(accs[c2+2] - accs[c2+1]),
                ang7 = Math.atan((accs[c2-2] - accs[c2-1]) / -1),
                ang8 = Math.atan(accs[c2] - accs[c2-1]);
                var an1 = ang1 - ang2,
                an2 = ang3 - ang4,
                an3 = ang5 - ang6,
                an4 = ang7 - ang8;
                var a1 = an1 + an2,
                a2 = an3 + an4;

                //console.log(c1 + ' => accs:' + accs[c1] + ' ang1:' + ang1 + ' ang2:' + ang2 + ' ang3:' + ang3 + ' ang4:' + ang4);
                //console.log(c2 + ' => accs:' + accs[c2] + ' ang5:' + ang5 + ' ang6:' + ang6 + ' ang7:' + ang7 + ' ang8:' + ang8);
                solution = a1 < a2 ? c1 : c2;
            }

            console.log("sol: " + solution);
            evt.emit("cracked", ""+solution);
        }
        //solution = diffs.reduce(function(prev, curr, index) {
            //if (curr > prev[0]) {
                //return [curr, index];
            //} else {
                //return prev;
            //}
        //}, [0, 0])[1];
        //var m = Number.MAX_VALUE;
        //for (var i = 0; i < diffs.length; i++) {
            //if (diffs[i] === true) {
                //var dp = accs[i-1] - accs[i-1];
                //var dn = accs[i+1] - accs[i+2];
                //if (accs[i] < m && dp < mp && dn < mn) {
                    //m = accs[i];
                    //mp = dp;
                    //mn = dn;
                    //solution = i;
                //}
            //}
        //}


        //var redDiff = colorDiff(colors, 0),
            //greenDiff = colorDiff(colors, 2);
        //if (redDiff < 10000 || greenDiff < 10000) {
            //type = 'spiral';
        //} else {
            //type = 'colors';
        //}
        
        //console.log(candidates(freqs, sums));

        //console.log(ans);
        //$('#chart').highcharts(({
            //title: {
                //text: 'Colors'
            //},
            ////colors: [
                ////'#ff0000', '#00ff00', '#0000ff'
            ////],
            //xAxis: {
                //categories: (new Array(max)).map(function(e, i) { return ''+i; })
            //},
            //yAxis: {
                //title: 'Sums',
                //plotLines: [{
                    //value: 0,
                    //width: 1,
                    //color: '#808080'
                //}]
            //},
            //legend: {
                //layout: 'vertical',
                //align: 'right',
                //verticalAlign: 'middle',
                //borderWidth: 0
            //},
            //series: [{
                ////name: 'Red',
                ////data: sums.map(function(a) { return a[0]; })
            ////}, {
                ////name: 'Green',
                ////data: sums.map(function(a) { return a[1]; })
            ////}, {
                ////name: 'Blue',
                ////data: sums.map(function(a) { return a[2]; })
            ////}]
                //name: 'Angles',
                //data: accs
            //}]
        //}));
    };
    captcha.src = src;

    return evt;
}

var server = http.createServer(function(req, res) {
    var image = decodeURIComponent(req.url.replace("/?image=", ""));

    console.log("image: " + image);
    var sol = crack(image);
    sol.on("cracked", function(solution) {
        fs.appendFileSync('solutions.txt', solution + ' ' + image + "\n");
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end(solution);
    });
    sol.on('failure', function() {
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end('failure');
    });
});
server.listen(8087, '127.0.0.1');
