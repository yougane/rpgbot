var fs = require('fs'),
    EventEmitter = require("events").EventEmitter;

$(document).ready(function() {
    var proxy_list = fs.readFileSync('proxy.txt', 'utf8'),
        proxies = proxy_list.split(/\s+/).filter(function(str) {
            return str.length > 0;
        });

    $("#form").on("submit", function(e) {
        e.preventDefault();
        var url = $("#link").val(),
            evt = new EventEmitter(),
            i = 0, sent = 0, running = 0;

        evt.on('complete', function(index) {
            $("#sent").html(++sent);
            $("#running").html(--running);
            if (i < proxies.length) {
                vote(url, proxies[i++], index, evt);
                $("#running").html(++running);
            }
        });

        evt.on('error', function(index) {
            $("#running").html(--running);
            if (i < proxies.length) {
                vote(url, proxies[i++], index, evt);
                $("#running").html(++running);
            }
        });

        for (var j = 0; j < 5; j++) {
            vote(url, proxies[i++], j, evt);
            $("#running").html(++running);
        }
    });
});
