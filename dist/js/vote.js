var Spooky = require("spooky");

function vote(url, proxy, index, evt) {
    var prox = proxy.split(':'),
        proxy_addr = "", proxy_auth = "";
    proxy_addr = prox[0] + ":" + prox[1];
    if (prox.length == 4) {
        proxy_auth = prox[2] + ":" + prox[3];
    }

    var spooky = new Spooky({
        child: {
            "transport": "http",
            "command": process.env.CASPERJS,
            "port": 1337 + index,
            "disk-cache": "true",
            "web-security": "no",
            "disable-web-security": "true",
            "proxy": proxy_addr,
            "proxy-auth": proxy_auth,
            "cookies-file": "cookies/" + index + ".txt"
        },
        casper: {
            pageSettings: {
                loadImages: false,
                loadPlugins: false,
                webSecurityEnabled: false,
                localToRemoteUrlAccessEnabled: true
            },
            logLevel: "error",
            waitTimeout: 60000,
            onError: function(self, message) {
                self.emit('error', message);
                self.exit();
            },
            onResourceRequested: function(self, req) {
                if (req.url.indexOf('page=voteprotect') !== -1) {
                    console.log('Blocked!');
                    self.emit('error', 'voteprotect');
                    self.exit();
                }
            }
        }
    }, function (err) {
        if (err) {
            e = new Error("Failed to initialize SpookyJS");
            e.details = err;
            throw e;
        }

        spooky.start();
        spooky.userAgent('Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');
        spooky.then(function() {
            phantom.clearCookies();
        });
        spooky.thenOpen(url);

        //spooky.thenOpen(url, function() {
            //if (this.getCurrentUrl().indexOf('page=voteprotect') !== -1) {
                //this.emit('error');
            //}
        //});

        spooky.waitForSelector("#images img", function() {
            var src = this.getElementAttribute("#images img", "src");
            //this.capture('capture.png');
            
            this.evaluate(function(image) {
                var src = image;

                function eventFire(el, etype){
                    if (el.fireEvent) {
                        el.fireEvent('on' + etype);
                    } else {
                        var evObj = document.createEvent('Events');
                        evObj.initEvent(etype, true, false);
                        el.dispatchEvent(evObj);
                    }
                }

                function checkRefreshed() {
                    alert('checking...');
                    var img = document.querySelector('#images img');
                    if (img === undefined || img === null ||img.src === src) {
                        window.setTimeout(checkRefreshed, 500);
                    } else {
                        alert('refreshed !');
                        src = img.src;
                        crack();
                    }
                }

                function crack() {
                    if (src.indexOf('http:') === -1) {
                        src = 'http:' + src;
                    }
                    var url = "http://localhost:8087/?image="+encodeURIComponent(src),
                    input = document.querySelector('input[name="adscaptcha_response_field"]'),
                    submit = document.querySelector('input[value="Voter"]'),
                    solution = __utils__.sendAJAX(url);

                    if (solution.indexOf('failure') !== -1) {
                        var refresh = document.querySelector('a.refresh');
                        eventFire(refresh, 'click');
                        alert('clicked');
                        checkRefreshed();
                    } else {
                        alert('casper: ' + solution);
                        input.value = solution;
                        submit.click();
                        alert('sent');
                    }
                }

                crack();
            }, {image: src});
        });
        
        spooky.then(function() {
            this.echo(this.getCurrentUrl());
            //this.capture('sub.png');
        });

        //spooky.on('resource.received', function(resource) {
            ////if (location.indexOf('//dn1kz9oabe8ga.cloudfront.net/cap/') !== -1) {
                ////this.evaluate(function(image) {
                    ////var url = "http://localhost:8087/?image="+encodeURIComponent(image),
                    ////input = document.querySelector('input[name="adscaptcha_response_field"]'),
                    ////submit = document.querySelector('input[value="Voter"]'),
                    ////solution = __utils__.sendAJAX(url);

                    ////if (solution.indexOf('failed') !== -1) {
                        ////console.log('failed');
                        ////document.querySelector('a.refresh').click();
                    ////} else {
                        ////input.value = solution;
                        ////submit.click();
                    ////}
                ////}, {image: location});
            ////}

            //console.log(resource);
        //});

        spooky.waitFor(function check() {
            return this.getCurrentUrl().indexOf("page=vote") === -1;
        }, function then() {
            this.emit('submited');
        }, function timeout() {
            this.emit('error');
        }, 600000);
        
        //spooky.start('http://realip.info/api/p/realip.php', function() {
            //this.emit('content', this.getHTML());
        //});
        spooky.run();
    });

    //spooky.on('navigation.requested', function(url, type) {
        //if (url.indexOf('page=voteprotect') !== -1) {
            //evt.emit('error', index);
            //spooky.exit();
        //}
    //});

    spooky.on('submited', function() {
        console.log('submited!');
        evt.emit('complete', index);
    });

    spooky.on('content', function(c) {
        console.log(c);
        alert(c);
    });

    spooky.on('error', function(message) {
        evt.emit('error', index);
        console.log('error');
    });

    spooky.on('remote.alert', function(message) {
        console.log(message);
    });

    spooky.on('console', function(line) {
        console.log(line);
    });
}
