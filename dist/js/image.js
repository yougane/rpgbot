function takeImage(image, max, n) {
    var width = image.width,
        height = image.height,
        imgHeight = height / max;

    var canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = imgHeight;

    var ctx = canvas.getContext("2d");
    ctx.drawImage(image, 0, n*imgHeight, width, imgHeight, 0, 0, width, imgHeight);
    var imageData = ctx.getImageData(0, 0, width, imgHeight);

    return imageData;
}

function canny(src, dst) {
    var img_u8 = new jsfeat.matrix_t(src.width, src.height, jsfeat.U8C1_t),
        blur_radius = 2,
        low_treshold = 127,
        high_treshold = 127,
        r = blur_radius|0,
        kernel_size = (r+1) << 1;

    jsfeat.imgproc.grayscale(src.data, src.width, src.height, img_u8);
    jsfeat.imgproc.gaussian_blur(img_u8, img_u8, kernel_size, 0);
    jsfeat.imgproc.canny(img_u8, img_u8, low_treshold|0, high_treshold|0);

    var data_u32 = new Uint32Array(dst.data.buffer),
        alpha = (0xff << 24),
        i = img_u8.cols*img_u8.rows, pix = 0;
    while (--i >= 0) {
        pix = img_u8.data[i];
        data_u32[i] = alpha | (pix << 16) | (pix << 8) | pix;
    }
}

function rgb2hsv () {
    var rr, gg, bb,
        r = arguments[0] / 255,
        g = arguments[1] / 255,
        b = arguments[2] / 255,
        h, s,
        v = Math.max(r, g, b),
        diff = v - Math.min(r, g, b),
        diffc = function(c){
            return (v - c) / 6 / diff + 1 / 2;
        };

    if (diff === 0) {
        h = s = 0;
    } else {
        s = diff / v;
        rr = diffc(r);
        gg = diffc(g);
        bb = diffc(b);

        if (r === v) {
            h = bb - gg;
        } else if (g === v) {
            h = (1 / 3) + rr - bb;
        } else if (b === v) {
            h = (2 / 3) + gg - rr;
        }
        if (h < 0) {
            h += 1;
        } else if (h > 1) {
            h -= 1;
        }
    }
    return {
        h: h,
        s: s,
        v: v
    };
}

//function hsv2rgb(h, s, v) {
    //var r, g, b, i, f, p, q, t;
    //if (h && s === undefined && v === undefined) {
        //s = h.s, v = h.v, h = h.h;
    //}
    //i = Math.floor(h * 6);
    //f = h * 6 - i;
    //p = v * (1 - s);
    //q = v * (1 - f * s);
    //t = v * (1 - (1 - f) * s);
    //switch (i % 6) {
        //case 0: r = v, g = t, b = p; break;
        //case 1: r = q, g = v, b = p; break;
        //case 2: r = p, g = v, b = t; break;
        //case 3: r = p, g = q, b = v; break;
        //case 4: r = t, g = p, b = v; break;
        //case 5: r = v, g = p, b = q; break;
    //}
    //return {
        //r: Math.floor(r * 255),
        //g: Math.floor(g * 255),
        //b: Math.floor(b * 255)
    //};
//}

//function pureColor(hue) {
    ////var h = Math.round(hue * 360);
    ////if (h < 60) return 0;
    ////if (h < 120) return 1/6;
    ////if (h < 180) return 2/6;
    ////if (h < 240) return 3/6;
    ////if (h < 300) return 4/6;
    ////if (h < 360) return 5/6;
    ////return 0;
    
    //return Math.floor(hue * 6) / 6;
//}

function colorDiff(colors, index) {
    var min = Number.MAX_VALUE, max = 0;

    for (var i = 0; i < colors.length; i++) {
        var freq = colors[i][index];
        if (freq < min) {
            min = freq;
        }
        if (freq > max) {
            max = freq;
        }
    }

    return max - min;
}

function isColoredCaptcha(freqs) {
    for (var i = 0; i < 3; i++) {
        var diff = colorDiff(freqs, i);
        if (diff < 10000) {
            return false;
        }
    }
    
    return true;
}

function maxIndex(arr) {
    var max = 0, index = 0;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] > max) {
            max = arr[i];
            index = i;
        }
    }

    return index;
}

function colorsFreqs(imageData) {
    var pixels = imageData.data,
        freqs = new Int32Array(3);

    for (var i = 0; i < pixels.length; i += 4) {
        var red = pixels[i],
            green = pixels[i+1],
            blue = pixels[i+2];
        
        var max = maxIndex([red, green, blue]);
        freqs[max] += 1;
    }
    
    return freqs;
}

function candidates(freqs, sums) {
    var maxs = freqs.map(function(a) {
        return maxIndex(a);
    });

    var c = maxs.reduce(function(prev, curr, index) {
        if (prev.value === 0 && curr !== 0) {
            prev.acc.push(index - 1);
        }
        return { value: curr, acc: prev.acc };
    }, {value: undefined, acc: []});

    var cands = c.acc.reduce(function(prev, curr) {
        if (sums[curr][2] < sums[prev][2]) {
            return curr;
        }
        return prev;
    });

    return cands;
}

function colorsSums(imageData) {
    var pixels = imageData.data,
        sums = new Int32Array(3);

    for (var i = 0; i < pixels.length; i += 4) {
        var red = pixels[i],
            green = pixels[i+1],
            blue = pixels[i+2];
        
        sums[0] += red;
        sums[1] += green;
        sums[2] += blue;
    }

    return sums;
}

function imageMap(pixels) {
    var map = new Uint8ClampedArray(pixels.length / 4);

    for (var i = 0; i < pixels.length; i += 4) {
        if (pixels[i] !== 0) {
            map[i/4] = 1;
        }
    }

    return map;
}

function lineLength(map, i, j, width, height) {
    if (i < 0 || i == height || j < 0 || j == width || map[i*width + j] === 0) {
        return 0;
    }

    map[i*width + j] = 0;
    var lengths = [];
    lengths.push(lineLength(map, i-1, j-1, width, height));
    lengths.push(lineLength(map, i-1, j, width, height));
    lengths.push(lineLength(map, i-1, j+1, width, height));
    lengths.push(lineLength(map, i, j+1, width, height));
    lengths.push(lineLength(map, i+1, j+1, width, height));
    lengths.push(lineLength(map, i+1, j, width, height));
    lengths.push(lineLength(map, i+1, j-1, width, height));
    lengths.push(lineLength(map, i, j-1, width, height));

    var len = 1 + lengths.reduce(function(prev, curr) {
        if (curr > prev) {
            return curr;
        } else {
            return prev;
        }
    });
    return len;
}

function longestLine(map, width, height) {
    var maxlength = 0;

    for (var i = 0; i < height; i++) {
        for (var j = 0; j < width; j++) {
            var index = i*width + j;
            var lengths = [];

            if (map[index] === 1) {
                map[index] = 0;
                lengths.push(lineLength(map, i-1, j-1, width, height));
                lengths.push(lineLength(map, i-1, j, width, height));
                lengths.push(lineLength(map, i-1, j+1, width, height));
                lengths.push(lineLength(map, i, j+1, width, height));
                lengths.push(lineLength(map, i+1, j+1, width, height));
                lengths.push(lineLength(map, i+1, j, width, height));
                lengths.push(lineLength(map, i+1, j-1, width, height));
                lengths.push(lineLength(map, i, j-1, width, height));

                var len = 1 + lengths.reduce(function(prev, curr) {
                    if (curr > prev) {
                        return curr;
                    } else {
                        return prev;
                    }
                });

                if (len > maxlength) {
                    maxlength = len;
                }
            }

        }
    }

    return maxlength;
}

//function angles(pixels, width, height) {
    //var sum1 = 0, sum2 = 0;

    //for (var i = 0; i < height; i++) {
        //for (var j = 0; j < Math.floor(width/2); j++) {
            //var p = i*width + j*4;
            //var pix = pixels[p] * 256;
            //pix += pixels[p+1] * 256;
            //pix += pixels[p+2];
            //if (pix !== 0) {
                //sum1++;
            //}
        //}
    //}

    //for (var i = 0; i < height; i++) {
        //for (var j = Math.floor(width/2); j < width; j++) {
            //var p = i*width + j*4;
            //var pix = pixels[p] * 256;
            //pix += pixels[p+1] * 256;
            //pix += pixels[p+2];
            //if (pix !== 0) {
                //sum2++;
            //}
        //}
    //}

    //return sum1 - sum2;
//}

function angles(pixels, width, height) {
    var sum = 0;

    for (var i = height-1; i >= 0; i--) {
        var y = height - i - height/2;
        for (var j = 0; j < width; j++) {
            var x = j - width/2;
            var p = i*width + j*4;
            var pix = pixels[p] * 256;
            pix += pixels[p+1] * 256;
            pix += pixels[p+2];
            if (pix !== 0) {
                sum += Math.abs(Math.atan2(y, x));
            }
        }
    }

    return sum;
}

function verticalDiff(pixels, width, height) {
    var h1 = 0,
        h2 = 0;

    for (var i = 0; i < height; i++) {
        for (var j = 0; j < Math.floor(width/2); j++) {
            var p = i*width + j*4;
            //var pix = pixels[p] * 256;
            //pix += pixels[p+1] * 256;
            //pix += pixels[p+2];
            if (pixels[p] !== 0) {
                h1 += height - i;
            }
        }
    }

    for (var i = 0; i < height; i++) {
        for (var j = Math.floor(width/2); j < width; j++) {
            var p = i*width + j*4;
            //var pix = pixels[p] * 256;
            //pix += pixels[p+1] * 256;
            //pix += pixels[p+2];
            if (pixels[p] !== 0) {
                h2 += height - i;
            }
        }
    }

    return h1 - h2;
}

function realsol(sums) {
    var diffs = [];

    //for (var i = 0; i < sums.length; i++) {
        //var diff;
        //if (i === 0) {
            //diff = sums[i] - sums[i+1];
        //} else if (i == sums.length - 1) {
            //diff = sums[i] - sums[i-1];
        //} else {
            //diff = (sums[i] - sums[i-1]) + (sums[i] - sums[i+1]);
        //}
        //diffs.push(diff);
    //}

    for (var i = 0; i < sums.length; i++) {
        var diff;
        if (i < 2 || i > (sums.length - 3)) {
            diff = false;
        } else {
            diff = ((sums[i] - sums[i-1]) > 0) && ((sums[i] - sums[i+1]) > 0)/* && (((sums[i-1] - sums[i-2]) * (sums[i+1] - sums[i+2])) < 100)*/;
        }
        diffs.push(diff);
    }

    return diffs;
}

function angle(x1, y1, x2, y2, x3, y3, x4, y4) {
    //var a = Math.atan2(y2 - y1, x2 - x1);
    //if (a < 0) {
        //a += 2 * Math.PI;
    //}
    //var a = Math.atan((y1 - y2) / (x1 - x2));
    var l1 = Math.sqrt(Math.pow(x2-x1, 2) + Math.pow(y2-y1, 2)),
        l2 = Math.sqrt(Math.pow(x4-x3, 2) + Math.pow(y4-y3, 2));
        a = Math.acos(l1 / l2);
    return a * (180 / Math.PI);
}

function sobelSum(imageData) {
    var sum = 0;

    var canvas = document.createElement('canvas');
    canvas.width = imageData.width;
    canvas.height = imageData.height;
    var ctx = canvas.getContext('2d');
    var data = ctx.createImageData(imageData);
    var pixels = new Uint8ClampedArray(data.data.length);
    var colors = new Int32Array(3);
    
    canny(imageData, data);
    
    //for (var j = 0; j < data.data.length; j += 4) {
        //var red = imageData.data[j],
            //green = imageData.data[j+1],
            //blue = imageData.data[j+2];
        ////var hsv = rgb2hsv(red, green, blue),
            ////pix = hsv2rgb(pureColor(hsv.h), 1, 1);
        //////console.log('RGB: ' + red + ',' + green + ',' + blue + '  HSV: ' + JSON.stringify(hsv));
        ////pixels[j] = pix.r;
        ////pixels[j+1] = pix.g;
        ////pixels[j+2] = pix.b;
        ////pixels[j+3] = 255;

        ////colors[Math.floor(hsv.h * 6)] += 1;
        
        //colors[0] += red;
        //colors[1] += green;
        //colors[2] += blue;
    //}
    //data.data.set(pixels);
    ctx.putImageData(data, 0, 0);
    //var img = new Image();
    //img.src = canvas.toDataURL();
    //document.body.appendChild(img);

    for (var i = 0; i < data.data.length; i++) {
        sum += data.data[i];
    }

    return {
        sum: sum,
        colors: colors,
        //ans: longestLine(imageMap(data.data), data.width, data.height)
        //ans: angles(data.data, data.width, data.height)
        //ans: verticalDiff(data.data, data.width, data.height) 
    };
}
